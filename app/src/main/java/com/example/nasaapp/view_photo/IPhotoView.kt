package com.example.nasaapp.view_photo

interface IPhotoView {
    fun viewPhoto()
    fun viewDesc()
}