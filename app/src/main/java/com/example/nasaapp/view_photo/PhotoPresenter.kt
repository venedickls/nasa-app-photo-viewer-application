package com.example.nasaapp.view_photo


class PhotoPresenter(private val photoView:IPhotoView) : IPhotoPresenter {
    override fun loadData() {
        photoView.viewPhoto()
        photoView.viewDesc()
    }
}