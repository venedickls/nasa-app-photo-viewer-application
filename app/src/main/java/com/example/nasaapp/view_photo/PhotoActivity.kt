package com.example.nasaapp.view_photo

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.nasaapp.R

class PhotoActivity : AppCompatActivity() {

    private lateinit var view: View
    private lateinit var photoView:PhotoView
    @SuppressLint("InflateParams")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        view = layoutInflater.inflate(R.layout.activity_photo, null)
        setContentView(view)
        photoView = PhotoView(this)
        photoView.viewData()
    }
}
























