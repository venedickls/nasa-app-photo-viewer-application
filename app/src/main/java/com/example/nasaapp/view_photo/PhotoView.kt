package com.example.nasaapp.view_photo

import android.app.Activity
import com.bumptech.glide.Glide
import com.example.nasaapp.methods.Photos
import kotlinx.android.synthetic.main.activity_photo.*

class PhotoView(private val activity: Activity) : IPhotoView {

    private var selectedPhotos: Photos? = activity.intent.getSerializableExtra(PHOTO_KEY) as Photos
    private lateinit var presenter:PhotoPresenter
    fun viewData(){
        presenter  = PhotoPresenter(this)
        presenter.loadData()
    }

    companion object {
        private val PHOTO_KEY = "PHOTO"
    }

    override fun viewPhoto() {
        Glide.with(activity).load(selectedPhotos?.url).into(activity.photoImageView)
    }

    override fun viewDesc() {
        activity.photoDescription.text = selectedPhotos?.explanation
    }
}