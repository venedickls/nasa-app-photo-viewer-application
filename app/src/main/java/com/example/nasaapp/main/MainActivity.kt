package com.example.nasaapp.main

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.example.nasaapp.R
import com.example.nasaapp.methods.ImageRequester
import com.example.nasaapp.methods.Photos

class MainActivity : AppCompatActivity(),ImageRequester.ImageRequesterResponse{
    override fun receivedPhoto(newPhoto: Photos) {
        runOnUiThread{
            mainView.photosList.add(newPhoto)
            mainView.adapter.notifyItemInserted(mainView.photosList.size-1)
        }
    }

    private lateinit var view: View
    private lateinit var mainView: MainView
    @SuppressLint("InflateParams")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        view = layoutInflater.inflate(R.layout.activity_main,null)
        setContentView(view)
        mainView = MainView(this)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main,menu)
        return true

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_change_recycler_manager) {
            mainView.changeLayoutManager()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
        mainView.start()
    }

}
