package com.example.nasaapp.main

interface IMainPresenter {
    fun loadData()
}