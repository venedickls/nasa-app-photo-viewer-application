package com.example.nasaapp.main

import android.app.Activity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import com.example.nasaapp.adapter.RecyclerAdapter
import com.example.nasaapp.methods.ImageRequester
import com.example.nasaapp.methods.Photos
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException

class MainView(private val activity: Activity) : IMainView{
    private var activityNew: Activity = activity

    var photosList: ArrayList<Photos> = ArrayList()
    private var imageRequester: ImageRequester
    private var linearLayoutManager: LinearLayoutManager = LinearLayoutManager(activity.applicationContext,LinearLayoutManager.VERTICAL,false)
    var adapter: RecyclerAdapter = RecyclerAdapter(photosList)
    private var gridLayoutManager: GridLayoutManager = GridLayoutManager(activity.applicationContext,2)
    var presenter:MainPresenter = MainPresenter()
    init {
        imageRequester = ImageRequester(activityNew)
        activity.recyclerView.layoutManager = linearLayoutManager
        activity.recyclerView.adapter = adapter
        setRecyclerViewScrollListener()
        setRecyclerViewItemTouchListener()
    }

    private val lastVisibleItemPosition:Int
        get() = if(activity.recyclerView.layoutManager == linearLayoutManager){
            linearLayoutManager.findLastVisibleItemPosition()
        }else{
            gridLayoutManager.findLastVisibleItemPosition()
        }

    override fun changeLayoutManager() {
        if(activity.recyclerView.layoutManager == linearLayoutManager){
            activity.recyclerView.layoutManager = gridLayoutManager
            if(photosList.size == 1){
                requestPhoto()
            }
        }else{
            activity.recyclerView.layoutManager = linearLayoutManager
        }
    }

    private fun setRecyclerViewItemTouchListener() {
        val itemTouchCallback = object: ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT){
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                val position = viewHolder.adapterPosition
                photosList.removeAt(position)
                activity.recyclerView.adapter!!.notifyItemRemoved(position)
            }
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, viewHolder1: RecyclerView.ViewHolder): Boolean {
                return false
            }
        }
        val itemTouchHelper = ItemTouchHelper(itemTouchCallback)
        itemTouchHelper.attachToRecyclerView(activity.recyclerView)
    }

    private fun setRecyclerViewScrollListener() {
        activity.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val totalItemCount = recyclerView.layoutManager!!.itemCount
                if(!imageRequester.isLoading && totalItemCount == lastVisibleItemPosition+1){
                    requestPhoto()
                }
            }
        })
    }
    fun requestPhoto(){
        try {
            imageRequester.getPhoto()
        }catch (e: IOException){
            e.printStackTrace()
        }
    }

    fun start(){
        if(photosList.size==0){
            requestPhoto()
        }
    }

}