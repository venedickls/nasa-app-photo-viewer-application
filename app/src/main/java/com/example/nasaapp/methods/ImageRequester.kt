package com.example.nasaapp.methods

import android.app.Activity
import android.content.Context
import android.net.Uri
import com.example.nasaapp.R
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*



class ImageRequester(activity: Activity) {
    interface ImageRequesterResponse{
        fun receivedPhoto(newPhoto: Photos)
    }

    private val calender:Calendar = Calendar.getInstance()
    private val dateFormat:SimpleDateFormat =  SimpleDateFormat("yyyy-MM-dd")
    private val responseListener: ImageRequesterResponse
    private val context : Context
    private val client: OkHttpClient
    var isLoading: Boolean = false
        private set

    init {
        responseListener = activity as ImageRequesterResponse
        context = activity.applicationContext
        client = OkHttpClient()
    }


    fun getPhoto(){
        val date = dateFormat.format(calender.time)
        val urlRequest = Uri.Builder().scheme(URL_SCHEME)
            .authority(URL_AUTHORITY)
            .appendPath(URL_PATH_1)
            .appendPath(URL_PATH_2)
            .appendQueryParameter(URL_QUERY_PARAM_DATE_KEY,date)
            .appendQueryParameter(
                URL_QUERY_PARAM_API_KEY,context.getString(
                    R.string.api_key
                ))
            .build().toString()

        val request = Request.Builder().url(urlRequest).build()
        isLoading = true

        client.newCall(request).enqueue(object:Callback{
            override fun onResponse(call: Call, response: Response) {
                try {
                    val photoJSON = JSONObject(response.body()!!.string())
                    calender.add(Calendar.DAY_OF_YEAR,-1)
                    if (photoJSON.getString(MEDIA_TYPE_KEY) != MEDIA_TYPE_VIDEO) {
                        val receivedPhoto = Photos(photoJSON)
                        responseListener.receivedPhoto(receivedPhoto)
                        isLoading = false
                    } else {
                        getPhoto()
                    }
                }catch (e: JSONException){
                    isLoading = false
                    e.printStackTrace()
                }

            }

            override fun onFailure(call: Call, e: IOException) {
                isLoading = false
                e.printStackTrace()
            }
        })
    }


    companion object {
        private val MEDIA_TYPE_KEY = "media_type"
        private val MEDIA_TYPE_VIDEO = "video"
        private val URL_SCHEME = "https"
        private val URL_AUTHORITY = "api.nasa.gov"
        private val URL_PATH_1 = "planetary"
        private val URL_PATH_2 = "apod"
        private val URL_QUERY_PARAM_DATE_KEY = "date"
        private val URL_QUERY_PARAM_API_KEY = "api_key"
    }
}