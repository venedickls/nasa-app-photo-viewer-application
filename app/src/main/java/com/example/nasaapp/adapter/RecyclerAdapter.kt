package com.example.nasaapp.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.nasaapp.R
import com.example.nasaapp.extensions.inflate
import com.example.nasaapp.methods.Photos
import com.example.nasaapp.view_photo.PhotoActivity
import kotlinx.android.synthetic.main.recyclerview_item_row.view.*


class RecyclerAdapter(private val photos: ArrayList<Photos>) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    class ViewHolder(private val view: View):RecyclerView.ViewHolder(view), View.OnClickListener {

        private var photos: Photos? = null

        init {
            view.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            val context = itemView.context
            val showPhotoIntent = Intent(context, PhotoActivity::class.java)
            showPhotoIntent.putExtra(PHOTO_KEY,photos)
            context.startActivity(showPhotoIntent)
            Log.d("RecyclerView","Clicked!")

        }
        companion object {
            private val PHOTO_KEY = "PHOTO"
        }

        fun bindPhoto(photos: Photos){
            this.photos = photos
            Glide.with(view.context)
                .load(photos.url).into(view.itemImage)
            view.itemDate.text = photos.humanDate
            view.itemDescription.text = photos.explanation

        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflatedView = parent.inflate(R.layout.recyclerview_item_row,false)
        return ViewHolder(inflatedView)
    }

    override fun getItemCount() = photos.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemPhoto = photos[position]
        holder.bindPhoto(itemPhoto)
    }
}